#!/usr/bin/env bash

set -o errexit nounset pipefail

function test_case {
    (
        cd "test-cases/$1"
        ../../result/snowflake build test-case
        ./verify.sh
    )
}

for test_case in test-cases/*; do
    test_case "$(basename "$test_case")"
done
