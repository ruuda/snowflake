// This file implements the Snowflake command-line tool. It executes the
// initialize.lua, the build file, and the finalize.lua files in order. It
// provides to these files a set of library functions, such as one for invoking
// nix-build.
//
// Do not free memory, especially the Lua state. Freeing memory takes time, and
// is not necessary if the program is immediately going to exit afterwards.

#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

int sf_nix_build(lua_State* state)
{
    char const* nixexpr = lua_tostring(state, 1);
    printf("%s", nixexpr);
    execlp(
        "nix-build",
        "nix-build",
        "--expr", nixexpr,
        NULL
    );
    return 0;
}

// Create a new Lua state or exit on error.
lua_State* sf_new_state(char const* target)
{
    lua_State* state = luaL_newstate();
    if (state == NULL)
    {
        perror("luaL_newstate");
        exit(1);
    }

    luaL_requiref(state, "_G", &luaopen_base, 1);
    luaL_requiref(state, "math", &luaopen_math, 1);
    luaL_requiref(state, "string", &luaopen_string, 1);
    luaL_requiref(state, "table", &luaopen_table, 1);
    luaL_requiref(state, "utf8", &luaopen_utf8, 1);
    lua_pop(state, 5);

    lua_newtable(state);

    lua_pushstring(state, "nix_build");
    lua_pushcfunction(state, &sf_nix_build);
    lua_settable(state, -3);

    lua_pushstring(state, "target");
    lua_pushstring(state, target);
    lua_settable(state, -3);

    lua_setglobal(state, "__snowflake");

    return state;
}

// Execute the initialize.lua file or exit on error.
void sf_initialize(lua_State* state)
{
    char source[] = {
        #include "../gen/initialize.lua.inc"
        , 0x00
    };
    int status = luaL_dostring(state, source);
    if (status != 0)
    {
        char const* error = lua_tostring(state, -1);
        fprintf(stderr, "initialize.lua: %s\n", error);
        exit(1);
    }
}

// Execute the build file or exit on error.
void sf_run_build_file(lua_State* state)
{
    int status = luaL_dofile(state, "snowflake.lua");
    if (status != 0)
    {
        char const* error = lua_tostring(state, -1);
        fprintf(stderr, "snowflake.lua: %s\n", error);
        exit(1);
    }
}

// Execute the finalize.lua file or exit on error.
void sf_finalize(lua_State* state)
{
    char source[] = {
        #include "../gen/finalize.lua.inc"
        , 0x00
    };
    int status = luaL_dostring(state, source);
    if (status != 0)
    {
        char const* error = lua_tostring(state, -1);
        fprintf(stderr, "finalize.lua: %s\n", error);
        exit(1);
    }
}

int main(int argc, char const* const* argv)
{
    if (argc != 3 || strcmp(argv[1], "build"))
    {
        fprintf(stderr, "Usage: snowflake build TARGET\n");
        exit(1);
    }

    lua_State* state = sf_new_state(argv[2]);
    sf_initialize(state);
    sf_run_build_file(state);
    sf_finalize(state);

    return 0;
}
