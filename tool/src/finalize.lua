if __snowflake.derivations[__snowflake.target] == nil then
    error("No such target: `" .. __snowflake.target .. "'")
end

do
    local nixexpr = ""

    nixexpr = nixexpr .. "let nixpkgs = (" .. __snowflake.nixpkgs .. ");\n"

    for dname, dnixexpr in pairs(__snowflake.derivations) do
        -- FIXME: dname should be escaped.
        -- FIXME: dnixexpr should be escaped.
        nixexpr = nixexpr .. "snowflake-" .. dname .. " = (" .. dnixexpr .. ");\n"
    end

    -- FIXME: target should be escaped.
    nixexpr = nixexpr .. "in snowflake-" .. __snowflake.target

    __snowflake.nix_build(nixexpr)
end
