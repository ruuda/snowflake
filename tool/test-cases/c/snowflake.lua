nixrule {
    name = "gcc",
    nixexpr = "nixpkgs.gcc",
}

genrule {
    name = "test-case",
    inputs = { "+hello-world.c", ":gcc" },
    outputs = { "@hello-world" },
    command = [[
        export PATH="$PATH:$(loc :gcc)/bin"
        cc -o "$(loc @hello-world)" "$(loc +hello-world.c)"
    ]],
}
