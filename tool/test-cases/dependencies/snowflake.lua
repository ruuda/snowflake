genrule {
    name = "foo",
    inputs = { "+foo.src" },
    outputs = { "@foo.out" },
    command = [[cat "$(loc +foo.src)" > "$(loc @foo.out)"]],
}

genrule {
    name = "bar",
    inputs = { "+bar.src" },
    outputs = { "@bar.out" },
    command = [[cat "$(loc +bar.src)" > "$(loc @bar.out)"]],
}

genrule {
    name = "test-case",
    inputs = { ":foo foo.out", ":bar bar.out" },
    outputs = { "@baz.out" },
    command = [[cat "$(loc :foo foo.out)" "$(loc :bar bar.out)" > "$(loc @baz.out)"]],
}
