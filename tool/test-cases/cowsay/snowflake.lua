nixrule {
    name = "cowsay",
    nixexpr = "nixpkgs.cowsay",
}

genrule {
    name = "test-case",
    inputs = { "+message.txt", ":cowsay" },
    outputs = { "@message-cowsay.txt" },
    command = [[
        export PATH="$PATH:$(loc :cowsay)/bin"
        cowsay < "$(loc +message.txt)" > "$(loc @message-cowsay.txt)"
    ]],
}
